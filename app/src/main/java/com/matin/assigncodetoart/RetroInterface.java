package com.matin.assigncodetoart;


import com.matin.assigncodetoart.pojos.MovieDetailsPojo;
import com.matin.assigncodetoart.pojos.MovieImageMainPojo;
import com.matin.assigncodetoart.pojos.MovieListMainPojo;
import com.matin.assigncodetoart.pojos.MovieListPojo;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface RetroInterface
{

    @GET("/3/movie/upcoming")
    Call<MovieListMainPojo> getMovieList(@Query("api_key") String api_key);

    @GET("/3/movie/{movie_id}")
    Call<MovieDetailsPojo>getSingleMovie(@Path("movie_id") int movie_id,
                                         @Query("api_key") String api_key);

    @GET("/3/movie/{movie_id}/images")
    Call<MovieImageMainPojo>getImages(@Path("movie_id") int movie_id,
                                           @Query("api_key") String api_key);

}

  //  @Header("Authorization") String Authorization


//https://image.tmdb.org/t/p/w500/pKESfn2Pdy0b7drvZHQb7UzgqoY.jpg