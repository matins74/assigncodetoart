package com.matin.assigncodetoart.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by FrameworkAndroid on 10/3/2017.
 */

public class MovieListMainPojo
{
    @SerializedName("results")
    @Expose
    private List<MovieListPojo> results = null;


    public List<MovieListPojo> getResults() {
        return results;
    }

    public void setResults(List<MovieListPojo> results) {
        this.results = results;
    }

}
