package com.matin.assigncodetoart.pojos;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by FrameworkAndroid on 10/4/2017.
 */

public class MovieImageMainPojo
{
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("backdrops")
    @Expose
    private List<PosterPojo> posters = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public List<PosterPojo> getPosters() {
        return posters;
    }

    public void setPosters(List<PosterPojo> posters) {
        this.posters = posters;
    }
}
