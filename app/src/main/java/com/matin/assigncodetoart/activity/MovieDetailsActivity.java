package com.matin.assigncodetoart.activity;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.matin.assigncodetoart.R;
import com.matin.assigncodetoart.RetroApiClient;
import com.matin.assigncodetoart.RetroInterface;
import com.matin.assigncodetoart.adapters.TopBannerPagerAdapter;
import com.matin.assigncodetoart.pojos.MovieDetailsPojo;
import com.matin.assigncodetoart.pojos.MovieImageMainPojo;
import com.matin.assigncodetoart.utils.Constants;

import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import me.relex.circleindicator.CircleIndicator;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MovieDetailsActivity extends AppCompatActivity {

    @BindView(R.id.movietitle)TextView movietitle;
    @BindView(R.id.movieDesc)TextView movieDesc;
    @BindView(R.id.pagerTopBanner)AutoScrollViewPager pagerTopBanner;
    @BindView(R.id.indicator)CircleIndicator indicator;
    @BindView(R.id.ratingBar)RatingBar ratingBar;
    RetroInterface retroInterface;
    String TAG = "MovieDetailsActivity";
    int movie_id;
    ArrayList<HashMap<String,String>> arrayListTopBannerImages;
    TopBannerPagerAdapter topBannerPagerAdapter;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_details);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        try {
            Bundle b= getIntent().getExtras();
            movie_id = b.getInt("movie_id");
        }catch (Exception e)
        {
            e.printStackTrace();
        }

        arrayListTopBannerImages = new ArrayList<>();
        topBannerPagerAdapter = new TopBannerPagerAdapter(this,arrayListTopBannerImages);
        pagerTopBanner.setAdapter(topBannerPagerAdapter);

        loadImages();
        loadMovieDetails();


    }

    private void loadImages() {
        progressDialog.show();
        retroInterface = RetroApiClient.getClient().create(RetroInterface.class);
        Call <MovieImageMainPojo>call = retroInterface.getImages(movie_id,Constants.API_KEY);
        call.enqueue(new Callback<MovieImageMainPojo>() {
            @Override
            public void onResponse(Call<MovieImageMainPojo> call, Response<MovieImageMainPojo> response) {
                Log.d(TAG,"Responce code is "+response.code()+"");

                for(int i =0;i<5;i++)
                {
                    HashMap<String,String>map=new HashMap<String, String>();

                    String image = response.body().getPosters().get(i).getFilePath();
                    map.put("image",image);

                    arrayListTopBannerImages.add(map);
                }
                topBannerPagerAdapter.notifyDataSetChanged();
                pagerTopBanner.startAutoScroll();
                pagerTopBanner.setInterval(4000);
                indicator.setViewPager(pagerTopBanner);
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MovieImageMainPojo> call, Throwable t) {
                progressDialog.dismiss();

                if (t instanceof UnknownHostException) {

                    Toast.makeText(MovieDetailsActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();
                }
                t.printStackTrace();
            }
        });
    }

    private void loadMovieDetails() {
        progressDialog.show();

        retroInterface = RetroApiClient.getClient().create(RetroInterface.class);
        Call<MovieDetailsPojo>call = retroInterface.getSingleMovie(movie_id, Constants.API_KEY);
        call.enqueue(new Callback<MovieDetailsPojo>() {
            @Override
            public void onResponse(Call<MovieDetailsPojo> call, Response<MovieDetailsPojo> response) {
                Log.d(TAG,"Responce code is "+response.code()+"");

                movietitle.setText(response.body().getTitle());
                movieDesc.setText(response.body().getOverview());
                double d = response.body().getVoteAverage()/2;
                ratingBar.setMax(5);
                ratingBar.setRating(Float.parseFloat(String.valueOf(d)));
                ratingBar.invalidate();
                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<MovieDetailsPojo> call, Throwable t) {
                progressDialog.dismiss();

                if (t instanceof UnknownHostException) {

                    Toast.makeText(MovieDetailsActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();
                }
                t.printStackTrace();
            }
        });
    }
}
