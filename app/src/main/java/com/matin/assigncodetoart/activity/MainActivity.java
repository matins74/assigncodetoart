package com.matin.assigncodetoart.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.matin.assigncodetoart.R;
import com.matin.assigncodetoart.RetroApiClient;
import com.matin.assigncodetoart.RetroInterface;
import com.matin.assigncodetoart.adapters.MovieListAdapter;
import com.matin.assigncodetoart.pojos.MovieListMainPojo;
import com.matin.assigncodetoart.pojos.MovieListPojo;
import com.matin.assigncodetoart.utils.Constants;

import java.net.UnknownHostException;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {

    @BindView(R.id.movieListRecyclerView)RecyclerView movieListRecyclerView;

    ArrayList<MovieListPojo> arrayListMovies;
    RetroInterface retroInterface;
    String TAG = "MainActivity";
    MovieListAdapter movieListAdapter;
    ProgressDialog progressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading...");
        progressDialog.setCancelable(false);


        movieListRecyclerView  = (RecyclerView)findViewById(R.id.movieListRecyclerView);
        movieListRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        arrayListMovies = new ArrayList<>();
        movieListAdapter = new MovieListAdapter(this, arrayListMovies);
        movieListRecyclerView.setAdapter(movieListAdapter);

        loadMovies();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_info)
        {
            Intent i = new Intent(MainActivity.this,InformationActivity.class);
            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }
    private void loadMovies() {

        progressDialog.show();
        retroInterface = RetroApiClient.getClient().create(RetroInterface.class);
        Call<MovieListMainPojo> call = retroInterface.getMovieList(Constants.API_KEY);
        call.enqueue(new Callback<MovieListMainPojo>() {
            @Override
            public void onResponse(Call<MovieListMainPojo> call, Response<MovieListMainPojo> response) {
                Log.d(TAG,"Responce code is "+response.code()+"");
                Log.d(TAG,"Responce status is "+response.body().getResults()+"");


                for(int i =0;i<response.body().getResults().size();i++)
                {

                    MovieListPojo movieListPojo = new MovieListPojo();

                    movieListPojo.setId(response.body().getResults().get(i).getId());
                    movieListPojo.setTitle(response.body().getResults().get(i).getTitle());
                    movieListPojo.setPosterPath(response.body().getResults().get(i).getPosterPath());
                    movieListPojo.setBackdropPath(response.body().getResults().get(i).getBackdropPath());
                    movieListPojo.setAdult(response.body().getResults().get(i).getAdult());
                    movieListPojo.setReleaseDate(response.body().getResults().get(i).getReleaseDate());

                    arrayListMovies.add(movieListPojo);
                }
                movieListAdapter.notifyDataSetChanged();
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<MovieListMainPojo> call, Throwable t) {
                progressDialog.dismiss();
                if (t instanceof UnknownHostException) {

                    Toast.makeText(MainActivity.this, "No internet connection", Toast.LENGTH_SHORT).show();
                }
                t.printStackTrace();
            }
        });
    }
}
