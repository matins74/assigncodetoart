package com.matin.assigncodetoart.utils;


import com.matin.assigncodetoart.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

/**
 * Created by DC on 7/19/2017.
 */

public class ImageShow
{
    public static DisplayImageOptions showImage()
    {
        DisplayImageOptions options = new DisplayImageOptions.Builder().cacheInMemory(true)
                .cacheOnDisc(true).resetViewBeforeLoading(true)
                .showImageForEmptyUri(R.mipmap.ic_launcher)
                .showImageOnFail(R.mipmap.ic_launcher)
                .showImageOnLoading(R.mipmap.ic_launcher).build();

        return options;
    }
}
