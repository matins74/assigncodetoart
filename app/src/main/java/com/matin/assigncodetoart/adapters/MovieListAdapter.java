package com.matin.assigncodetoart.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.matin.assigncodetoart.R;
import com.matin.assigncodetoart.activity.MainActivity;
import com.matin.assigncodetoart.activity.MovieDetailsActivity;
import com.matin.assigncodetoart.pojos.MovieListPojo;
import com.matin.assigncodetoart.utils.Constants;
import com.matin.assigncodetoart.utils.ImageShow;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by FrameworkAndroid on 10/3/2017.
 */

public class MovieListAdapter extends RecyclerView.Adapter<MovieListAdapter.ViewHolder> {

    private  Context context;
    private ArrayList<MovieListPojo> arrayListMovies;
    private ImageLoader imageLoader;

    public MovieListAdapter(Context context, ArrayList<MovieListPojo> arrayListMovies) {
            this.context = context;
        this.arrayListMovies = arrayListMovies;
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.sublayout_movie,parent,false);

        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        holder.movieName.setText(arrayListMovies.get(position).getTitle());
        holder.movieReleaseDate.setText(arrayListMovies.get(position).getReleaseDate());
        if(arrayListMovies.get(position).getAdult())
        {
            holder.movieAdult.setText("(A)");
        }
        else
        {
            holder.movieAdult.setText("(U/A)");
        }
        imageLoader.displayImage(Constants.BASE_URL_IMAGE + arrayListMovies.get(position).getPosterPath(),holder.movieImageView, ImageShow.showImage());
    }

    @Override
    public int getItemCount() {
        return arrayListMovies.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder
    {
        @BindView(R.id.movieName) public TextView movieName;
        @BindView(R.id.movieReleaseDate) public TextView movieReleaseDate;
        @BindView(R.id.movieAdult) public TextView movieAdult;
        @BindView(R.id.movieImageView) public ImageView movieImageView;

        public ViewHolder(View itemView) {
            super(itemView);

            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    int pos = getAdapterPosition();

                    Intent intent = new Intent(context, MovieDetailsActivity.class);
                    intent.putExtra("movie_id",arrayListMovies.get(pos).getId());
                    context.startActivity(intent);


                }
            });
        }

    }
}
