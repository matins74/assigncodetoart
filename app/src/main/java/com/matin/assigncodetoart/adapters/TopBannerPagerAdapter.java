package com.matin.assigncodetoart.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;


import com.matin.assigncodetoart.R;
import com.matin.assigncodetoart.utils.Constants;
import com.matin.assigncodetoart.utils.ImageShow;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by DC on 7/19/2017.
 */

public class TopBannerPagerAdapter extends PagerAdapter
{

    Context context;
    ArrayList<HashMap<String, String>> arrayListTopBannerImages;
    protected ImageLoader imageLoader;
    public TopBannerPagerAdapter(Context context, ArrayList<HashMap<String, String>> arrayListTopBannerImages) {
        this.context = context;
        this.arrayListTopBannerImages = arrayListTopBannerImages;
       // imageLoader.getInstance().init(ImageLoaderConfiguration.createDefault(context));
        imageLoader = ImageLoader.getInstance();

    }

    @Override
    public int getCount() {
        return arrayListTopBannerImages.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {

        LayoutInflater inflater = ((Activity)context).getLayoutInflater();

        View viewItem = inflater.inflate(R.layout.sublayout_topbanner, container, false);
        ImageView imageView = (ImageView) viewItem.findViewById(R.id.topBannerImageView);

        imageLoader.displayImage(Constants.BASE_URL_IMAGE+arrayListTopBannerImages.get(position).get("image"),imageView, ImageShow.showImage());
      //  Picasso.with(context).load("http://addekh.com/uploads/affilate_management/banner/2000x560/"+arrayListTopBannerImages.get(position).get("image")).into(imageView);

        container.addView(viewItem);
        return viewItem;
    }
    @Override
    public boolean isViewFromObject(View view, Object object) {
        return object == view;
    }
    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        // TODO Auto-generated method stub
        container.removeView((View) object);
    }
}
